import numpy as np

x = np.mgrid[-2:5:12j]
y = np.mgrid[1:9:12j]
z = np.mgrid[-5:3:12j]

data = np.concatenate((x[:, np.newaxis], y[:, np.newaxis], z[:, np.newaxis]), axis=1)

print(data)


x = [1, 2, 3, 4, 5]
y = [1, 2, 3, 4, 5]
z = [1, 2, 3, 4, 5]
x = np.array(x)
y = np.array(y)
z = np.array(z)
data = np.concatenate((x[:, np.newaxis], y[:, np.newaxis], z[:, np.newaxis]), axis=1)
print(data)