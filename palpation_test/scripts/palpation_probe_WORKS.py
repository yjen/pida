#!/usr/bin/python



# @@@@@@@@@@@@@@@@@@@@@@@@@@
"""
CHANGES NEED TO BE LINKED TO palpation_probe_from_file.py
"""
# @@@@@@@@@@@@@@@@@@@@@@@@@@





"""
1. find local peaks in signal by using second derivative and first derivative
2. get rid of points near edges of block (in x direction)
3. use 3d linear regression (first component of pca) to find line of best fit for occlusion
4. use 3d linear regression to find lines of best fit for first and last row of points
5. find the two points on line of best fit for occlusion closest to the other two lines; these are the start & end points
"""

# below irrelevant if we are not doing weighted regression
# wait how are we taking care of the baseline changing?
# we are assuming it doesn't change right?

from std_msgs.msg import String
from geometry_msgs.msg import PoseStamped
import rospy

from math import sqrt
import numpy as np
from numpy.polynomial.polynomial import polyfit
from numpy.linalg import lstsq
import matplotlib.pyplot as plot
import mpl_toolkits.mplot3d as m3d
import pickle

from closestPoint3D import closestDistanceBetweenLines

import tfx

NEIGHBORHOOD_SIZE = 0.001
BIN_SIZE_MIN = 3
BIN_SIZE_MAX = 9
bin_size = -1
SECOND_DER_CUTOFF = -0.0005
FIRST_DER_CUTOFF = 0.000125

"""
finds points with negative second derivative
"""
def find_local_maxima(data):
	second_der_indices = []
	h = bin_size #4 works for larger vein; 6 seems to be good for smaller vein. I guess 7+ recovers more points on the lines, but doesn't discover new lines
	for i in range(h, len(data) - h):
		second_der = (data[i + h] - (2 * data[i]) + data[i - h]) / (h * h)
		if second_der < SECOND_DER_CUTOFF:
			second_der_indices.append(i)

	peak_indices = []
	for i in second_der_indices:
		left_first_der = data[i - (h // 2)] - data[i - 2 * (h // 2)]
		right_first_der = data[i + 2 * (h // 2)] - data[i + (h // 2)]
		if left_first_der > FIRST_DER_CUTOFF and right_first_der < -1 * FIRST_DER_CUTOFF: # +-0.001 works for larger vein
			peak_indices.append(i) 

	return peak_indices


# getting the injection first
# doing multiple veins; first need to segment data into lines of palpation (use z info when probe lifts off, or pauses, or message from other pc)



# units are in meters




# haven't taken into account points of interest near the ends of the lines of palpation yet
# oh the problem was if we take the data from the vertical lines of palpation maybe
# let's just ignore data from the ends of the block


def find_poi(indices, pose_data, palpation_data):
	right_x = pose_data[0].pose.position.x
	left_x = pose_data[-1].pose.position.x
	#print(left_x)
	#print(right_x)
	"""
	x = [pose_data[i].pose.position.x for i in indices]
	y = [pose_data[i].pose.position.y for i in indices]
	plot.scatter(x, y)
	plot.show()
	while True:
		print('ha')
	"""
	
	print(indices)
	indices = [i for i in indices if pose_data[i].pose.position.x - left_x > 0.01 and right_x - pose_data[i].pose.position.x > 0.01]
	indices = [i for i in indices if palpation_data[i] >= 0]
	return indices


def calc_dist(p1, p2):
	
	p1 = p1.pose.position
	p2 = p2.pose.position
	return sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2) + pow(p1.z - p2.z, 2))


# this won't work if we cross multiple veins while palpating?
# also this doesn't fix the problem of the changing baseline for signal?


def fit_3d_line_pca(indices, pose_data):
	x = []
	y = []
	z = []
	for i in indices:
		pose = pose_data[i].pose.position
		x.append(pose.x)
		y.append(pose.y)
		z.append(pose.z)
	x = np.array(x)
	y = np.array(y)
	z = np.array(z)

	data = np.concatenate((x[:, np.newaxis], y[:, np.newaxis], z[:, np.newaxis]), axis=1)
	datamean = data.mean(axis=0)
	
	uu, dd, vv = np.linalg.svd(data - datamean)
	print(vv[0])
	return [vv[0], datamean, data]


def find_vein(indices, pose_data, palpation_data):
	
	print(indices)
	NUM_POINTS = 2
	# using 2 points to fit the lines
	first_four_indices = indices[0:NUM_POINTS]
	last_four_indices = indices[len(indices) - NUM_POINTS:len(indices)]
	vein_line = fit_3d_line_pca(indices, pose_data)
	first_row_line = fit_3d_line_pca(first_four_indices, pose_data)
	last_row_line = fit_3d_line_pca(last_four_indices, pose_data)

	
	# vv[0] gives params for line

	vein_p1 = vein_line[1]
	vein_p2 = vein_p1 + vein_line[0]
	first_row_p1 = first_row_line[1]
	first_row_p2 = first_row_p1 + first_row_line[0]
	last_row_p1 = last_row_line[1]
	last_row_p2 = last_row_p1 + last_row_line[0]

	starting_point = closestDistanceBetweenLines(vein_p1, vein_p2, first_row_p1, first_row_p2)[0]
	ending_point = closestDistanceBetweenLines(vein_p1, vein_p2, last_row_p1, last_row_p2)[0]

	
	# linepts1 = vein_line[0] * np.mgrid[-0.1:0.1:2j][:, np.newaxis]
	# print(vein_line[0])
	# print(vein_line[1])
	# linepts2 = first_row_line[0] * np.mgrid[-0.1:0.1:2j][:, np.newaxis]
	# linepts3 = last_row_line[0] * np.mgrid[-0.1:0.1:2j][:, np.newaxis]
	# linepts1 += vein_line[1]
	# linepts2 += first_row_line[1]
	# linepts3 += last_row_line[1]

	# ax = m3d.Axes3D(plot.figure())
	# ax.scatter3D(*vein_line[2].T)
	# print(linepts1)
	# ax.plot3D(*linepts1.T)
	# ax.plot3D(*linepts2.T)
	# ax.plot3D(*linepts3.T)
	# plot.show()
	
	return starting_point, ending_point
	



def estimate_vein_location(palpation_data, pose_data):
	global bin_size
	max_data = max(palpation_data)
	min_data = min(palpation_data)
	avg = 0
	for d in palpation_data:
		avg += d
	avg /= len(palpation_data)
	palpation_data = [(d - avg) / (max_data - min_data) for d in palpation_data]
	print(palpation_data)


	best_num_points = -1
	best_bin_size = -1
	poi = None
	for i in range(BIN_SIZE_MIN, BIN_SIZE_MAX + 1):
		bin_size = i
		local_maxima = find_local_maxima(palpation_data)
		print(local_maxima)
		cur_poi = find_poi(local_maxima, pose_data, palpation_data)
		print(str(len(cur_poi)) + " points")
		print(str(i) + " bin size")
		if len(cur_poi) > best_num_points:
			best_num_points = len(cur_poi)
			best_bin_size = i
			poi = cur_poi
	
	bin_size = best_bin_size
	print(bin_size)
	return find_vein(poi, pose_data, palpation_data)