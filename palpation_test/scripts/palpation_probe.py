#!/usr/bin/python

"""
1. find local peaks in signal by using second derivative and first derivative
2. get rid of points near edges of block (in x direction)
3. use 3d linear regression (first component of pca) to find line of best fit for occlusion
4. use 3d linear regression to find lines of best fit for first and last row of points
5. find the two points on line of best fit for occlusion closest to the other two lines; these are the start & end points
"""


# below irrelevant if we are not doing weighted regression
# wait how are we taking care of the baseline changing?
# we are assuming it doesn't change right?

from std_msgs.msg import String
from geometry_msgs.msg import PoseStamped
import rospy

from math import sqrt
import numpy as np
from numpy.polynomial.polynomial import polyfit
from numpy.linalg import lstsq
import matplotlib.pyplot as plot
import mpl_toolkits.mplot3d as m3d
import pickle

from closestPoint3D import closestDistanceBetweenLines

import tfx

NEIGHBORHOOD_SIZE = 0.001
BIN_SIZE_MIN = 3
BIN_SIZE_MAX = 9
SECOND_DER_CUTOFF = -0.0005 #-0.0001. 0
FIRST_DER_CUTOFF = 0.000125
NUM_POINTS = 2 # number of points used for fitting lines to first & last rows of data
"""
finds points with negative second derivative
"""
def find_local_maxima(data, h):
	second_der_indices = []
	for i in range(h, len(data) - h):
		second_der = (data[i + h] - (2 * data[i]) + data[i - h]) / (h * h)
		if second_der < SECOND_DER_CUTOFF:
			second_der_indices.append(i)

	peak_indices = []
	for i in second_der_indices:
		left_first_der = data[i - (h // 2)] - data[i - 2 * (h // 2)]
		right_first_der = data[i + 2 * (h // 2)] - data[i + (h // 2)]
		if left_first_der > FIRST_DER_CUTOFF and right_first_der < -1 * FIRST_DER_CUTOFF: # +-0.001 works for larger vein
			peak_indices.append(i) 

	return peak_indices


# getting the injection first
# doing multiple veins; first need to segment data into lines of palpation (use z info when probe lifts off, or pauses, or message from other pc)

# clustering/outlier (singletons) detection to remove noise. can do iteratively until line of best fit has high coefficient
# can also tune max detection parameters iteratively until line of best fit has high coefficient

# units are in meters




# haven't taken into account points of interest near the ends of the lines of palpation yet
# oh the problem was if we take the data from the vertical lines of palpation maybe
# let's just ignore data from the ends of the block


def find_poi(indices, pose_data, palpation_data, graph_flag):
	right_x = pose_data[0].pose.position.x
	left_x = pose_data[-1].pose.position.x
	print("x-coordinate of left side of block: " + str(left_x))
	print("x-coordinate of right side of block: " + str(right_x))

	if graph_flag:
		x = [pose_data[i].pose.position.x for i in indices]
		y = [pose_data[i].pose.position.y for i in indices]
		z = [palpation_data[i] for i in indices]

		x = np.array(x)
		y = np.array(y)
		z = np.array(z)
		ax = m3d.Axes3D(plot.figure())
		ax.set_zlabel('normalized signal')
		ax.set_ylabel('y')
		ax.set_xlabel('x')
		data = np.concatenate((x[:, np.newaxis], y[:, np.newaxis], z[:, np.newaxis]), axis=1)
		ax.scatter3D(*data.T)
		plot.show()
	
	print("indices: " + str(indices))
	indices = [i for i in indices if pose_data[i].pose.position.x - left_x > 0.01 and right_x - pose_data[i].pose.position.x > 0.01]
	indices = [i for i in indices if palpation_data[i] >= 0]

	if graph_flag:
		print("INDICES OF INTEREST: " + str(indices))
		indices_data_file = open('indices.p', 'wb+')
		pickle.dump(indices, indices_data_file)
		indices_data_file.close()

	if graph_flag:
		x = [pose_data[i].pose.position.x for i in indices]
		y = [pose_data[i].pose.position.y for i in indices]
		z = [palpation_data[i] for i in indices]

		x = np.array(x)
		y = np.array(y)
		z = np.array(z)
		ax = m3d.Axes3D(plot.figure())
		ax.set_zlabel('normalized signal')
		ax.set_ylabel('y')
		ax.set_xlabel('x')

		data = np.concatenate((x[:, np.newaxis], y[:, np.newaxis], z[:, np.newaxis]), axis=1)
		ax.scatter3D(*data.T)
		plot.show()

	return indices

	poi_indices = []
	for i in indices:
		pose = pose_data[i]
		l = i - 1
		r = i + 1

		if l >= 0:
			left_dist = calc_dist(pose, pose_data[l])
			while left_dist <= NEIGHBORHOOD_SIZE:
				l -= 1
				if l < 0:
					break
				left_dist = calc_dist(pose, pose_data[l])

		if r <= len(pose_data) - 1:		
			right_dist = calc_dist(pose, pose_data[r])
			while right_dist <= NEIGHBORHOOD_SIZE:
				r += 1
				if r > len(pose_data) - 1:
					break
				right_dist = calc_dist(pose, pose_data[r])

		poi_indices.extend(list(range(l + 1, r)))
	poi_indices = list(set(poi_indices))

	if graph_flag:
		x = [pose_data[i].pose.position.x for i in poi_indices]
		y = [pose_data[i].pose.position.y for i in poi_indices]
		z = [palpation_data[i] for i in poi_indices]

		x = np.array(x)
		y = np.array(y)
		z = np.array(z)
		ax = m3d.Axes3D(plot.figure())

		data = np.concatenate((x[:, np.newaxis], y[:, np.newaxis], z[:, np.newaxis]), axis=1)
		ax.scatter3D(*data.T)
		plot.show()

	return poi_indices


def calc_dist(p1, p2):
	p1 = p1.pose.position
	p2 = p2.pose.position
	return sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2) + pow(p1.z - p2.z, 2))


# this won't work if we cross multiple veins while palpating?
# also this doesn't fix the problem of the changing baseline for signal?


def fit_3d_line_pca(indices, pose_data):
	x = []
	y = []
	z = []
	for i in indices:
		pose = pose_data[i].pose.position
		x.append(pose.x)
		y.append(pose.y)
		z.append(pose.z)
	x = np.array(x)
	y = np.array(y)
	z = np.array(z)

	data = np.concatenate((x[:, np.newaxis], y[:, np.newaxis], z[:, np.newaxis]), axis=1)
	datamean = data.mean(axis=0)
	
	uu, dd, vv = np.linalg.svd(data - datamean)


	vein_p1 = np.array(vv[0][1])
	vein_p2 = np.array(vein_p1 + vv[0][0])


	"""
	sum_errors = 0
	for i in indices:
		pose = pose_data[i].pose.position
		p0 = np.array([pose.x, pose.y, pose.z])
		term1 = p0 - vein_p1
		term2 = p0 - vein_p2
		term3 = vein_p2 - vein_p1
		numerator = np.linalg.norm(np.cross(term1, term2))
		denominator = np.linalg.norm(term3)
		sum_errors += numerator / denominator
		print(str(numerator / denominator) + " indiv error")
	print(str(len(indices)) + " points" )
	"""

		#x1, x2 are points on line; x0 is the standalone point
		#|(x0-x1) x (x0-x2)|/|x2-x1|
	
	#0.26 indiv
	#13.53
	print(vv[0])
	#print(str(sum_errors) + " sum_errors")
	return [vv[0], datamean, data]#, sum_errors


def find_vein(indices, pose_data, palpation_data, graph_flag):
	print("indices: " + str(indices))
	
	# using 2 points to fit the lines
	first_four_indices = indices[0:NUM_POINTS]
	last_four_indices = indices[len(indices) - NUM_POINTS:len(indices)]
	vein_line = fit_3d_line_pca(indices, pose_data)
	first_row_line = fit_3d_line_pca(first_four_indices, pose_data)
	last_row_line = fit_3d_line_pca(last_four_indices, pose_data)

	
	# vv[0] gives params for line

	vein_p1 = vein_line[1]
	vein_p2 = vein_p1 + vein_line[0]
	first_row_p1 = first_row_line[1]
	first_row_p2 = first_row_p1 + first_row_line[0]
	last_row_p1 = last_row_line[1]
	last_row_p2 = last_row_p1 + last_row_line[0]

	starting_point = closestDistanceBetweenLines(vein_p1, vein_p2, first_row_p1, first_row_p2)[0]
	ending_point = closestDistanceBetweenLines(vein_p1, vein_p2, last_row_p1, last_row_p2)[0]

	if graph_flag:
		linepts1 = vein_line[0] * np.mgrid[-0.1:0.1:2j][:, np.newaxis]
		print(vein_line[0])
		print(vein_line[1])
		linepts2 = first_row_line[0] * np.mgrid[-0.1:0.1:2j][:, np.newaxis]
		linepts3 = last_row_line[0] * np.mgrid[-0.1:0.1:2j][:, np.newaxis]
		linepts1 += vein_line[1]
		linepts2 += first_row_line[1]
		linepts3 += last_row_line[1]

		ax = m3d.Axes3D(plot.figure())
		ax.set_zlabel('normalized signal')
		ax.set_ylabel('y')
		ax.set_xlabel('x')
		#plot.gca().set_aspect('equal')
		ax.scatter3D(*vein_line[2].T)
		print(linepts1)
		ax.plot3D(*linepts1.T)
		ax.plot3D(*linepts2.T, color='orange', alpha=100)
		ax.plot3D(*linepts3.T)
		plot.show()


	# fig = plot.figure(figsize=(12,12))
	#   x = range(len(palpation_data))
	#   w = palpation_data
	#   y = x
	#	z = x
	#   ax = fig.add_subplot(511)
	#   aw = fig.add_subplot(512)
	#   a1 = fig.add_subplot(513)
	#   a2 = fig.add_subplot(514)
	#   a3 = fig.add_subplot(515)
	#   ly, = ax.plot(x, y)
	#   lz, = ax.plot(x, z)
	#   lw, = aw.plot(x, w)
	#   la1, = a1.plot(x, w)
	#   la2, = a2.plot(x, w)
	#   la3, = a3.plot(x, w)
	#   aw.set_ylim([9000, 12000])
	#   a3.set_ylim([0, 3000])
	#   a2.set_ylim([3000, 6000])
	#   a1.set_ylim([6000, 9000])
	#   # draw and show it
	#   fig.canvas.draw()
	#   plot.show(block=False)

	
	return starting_point, ending_point
	



def estimate_vein_location(palpation_data, pose_data, graph_flag):
	max_data = max(palpation_data)
	min_data = min(palpation_data)
	avg = 0
	for d in palpation_data:
		avg += d
	avg /= len(palpation_data)
	palpation_data = [(d - avg) / (max_data - min_data) for d in palpation_data]
	print(palpation_data)

	best_num_points = -1
	best_bin_size = -1
	#poi = None
	for i in range(BIN_SIZE_MIN, BIN_SIZE_MAX + 1):
		local_maxima = find_local_maxima(palpation_data, i)
		print(local_maxima)
		poi = find_poi(local_maxima, pose_data, palpation_data, False)
		num_points = len(poi)
		print(str(num_points) + " points")
		print(str(i) + " bin size")
		if num_points > best_num_points:
			best_num_points = num_points
			best_bin_size = i
			#poi = cur_poi
	local_maxima = find_local_maxima(palpation_data, best_bin_size)
	poi = find_poi(local_maxima, pose_data, palpation_data, graph_flag)
	print("best bin size: " + str(best_bin_size))
	return find_vein(poi, pose_data, palpation_data, graph_flag)

if __name__ == '__main__':
	file_name = str(raw_input("Type in name of file (do not include prefix or extension): "))
	if len(file_name) < 3:
		file_name = ''
	palp_file = open('palpation_data_' + file_name + '.csv', 'r')
	pose_file = open('pose_data_' + file_name + '.p', 'rb+')

	palp_data = palp_file.read().split(',')
	palp_data = [float(palp_data[i]) for i in range(len(palp_data)) if i != len(palp_data) - 1]
	pose_data = pickle.load(pose_file)

	start_end_points = estimate_vein_location(palp_data, pose_data, True)
	print(start_end_points)

	publisher_start = rospy.Publisher("/palpation/cut_start_point", PoseStamped)
	publisher_end = rospy.Publisher("/palpation/cut_end_point", PoseStamped)
	rospy.init_node('palpprobe', anonymous=True)

	start_pose = tfx.pose(start_end_points[0]) # x,y,z
	end_pose = tfx.pose(start_end_points[1])
	publisher_start.publish(start_pose.msg.PoseStamped())
	publisher_end.publish(end_pose.msg.PoseStamped())