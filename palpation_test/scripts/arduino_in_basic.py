#!/usr/bin/python
from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
import serial, time
import rospy
from std_msgs.msg import Int32, String
from geometry_msgs.msg import PoseStamped
import palpation_probe as pp
import pickle
import tfx


START = "start"
STOP = "stop"
THRESHOLD = 3700
WINDOW = 700
REFRESH_RATE = 10
RECORD_RATE = 10 # 1 out of 10 data points will be recorded
BUFFER_LENGTH = 50
ser = serial.Serial('/dev/ttyACM0', 9600)#, timeout=0)
#ser = serial.Serial('/dev/tty0', 9600)#, timeout=0)

def linear_scale(y):
    return (4096/4) * y

#deg 6 poly, coeffs highest to lowest
deg6 = [9.389e-21,1.684e-17,-8.124e-13,3.597e-09,-5.512e-06,0.003552,-0.3207]
def inverse(y):
    x = 0
    for coeff in deg6:
        x += coeff + x*y
    return x

def transform(elem):
    return linear_scale(inverse(elem))


def read1():
    data = ser.readline()
    while True:
        while data.strip() == '':
            data = ser.readline()
            print 'no data'
        data = data.strip().split(',')[1]
        data = data.strip().split('\r')
        result = [int(x) for x in data if x != '']
        if len(result) != 0:
            return result
    return None

def read():
    try:
        return read1()
    except (ValueError, IndexError) as e:
        return read()


palpation_data = []
pose_data = []
status = None
recording_pose = False
publisher_start = None
publisher_end = None

def record_status_callback(data):
    global publisher_start
    global publisher_end
    global palpation_data
    global pose_data
    global status
    status = data.data
    rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.data)
    
    if status == START:
        palpation_data = []
        pose_data = []
    elif status == STOP:
        # need to change this to publishing to a topic?
        print(palpation_data)
        print(pose_data)
        print(len(palpation_data))
        print(len(pose_data))
        palp_data_file = open('palpation_data', 'w')
        pose_data_file = open('pose_data', 'wb+')
        for d in palpation_data:
            palp_data_file.write(str(d) + ',')
        pickle.dump(pose_data, pose_data_file)
        pose_data_file.close()
        palp_data_file.close()

        
        combined_data = [(palpation_data[i], pose_data[i]) for i in range(len(palpation_data))]

        combined_data_file = open('combined_data', 'wb+')
        pickle.dump(combined_data, combined_data_file)
        combined_data_file.close()






        """
        palp_data_file = open('palpation_data', 'r')
        pose_data_file = open('pose_data', 'r')
        
        palpation_data = palp_data_file.read().split(',')
        palpation_data = [float(d) for d in palpation_data]

        pose_data = pickle.load(pose_data_file)
        """
        start_end_points = pp.estimate_vein_location(palpation_data, pose_data)

        start_pose = tfx.pose(start_end_points[0]) # x,y,z
        end_pose = tfx.pose(start_end_points[1])
        publisher_start.publish(start_pose.msg.PoseStamped())
        publisher_end.publish(end_pose.msg.PoseStamped())
        """
        local_maxima = pp.find_local_maxima(palpation_data)
        poi = pp.find_poi(local_maxima, pose_data)
        vein_location_estimate = pp.find_vein(poi, pose_data, palpation_data)
        """



def record_pose_callback(data):
    global pose_data
    global recording_pose
    if recording_pose:
        pose_data.append(data)
        recording_pose = False


def listener():
    global publisher_start
    global publisher_end
    global palpation_data
    global recording_pose
    publisher_start = rospy.Publisher("/palpation/cut_start_point", PoseStamped)
    publisher_end = rospy.Publisher("/palpation/cut_end_point", PoseStamped)
    rospy.init_node('arduino_in', anonymous=True)
    rospy.Subscriber("palpation_record", String, record_status_callback)
    # /dvrk_psm1/joint_position_cartesian
    rospy.Subscriber("dvrk_psm1/joint_position_cartesian", PoseStamped, record_pose_callback)
    fig = plt.figure(figsize=(12,12))
    #plt.ylim(-200, 6000)
    ax = fig.add_subplot(511)
    aw = fig.add_subplot(512)
    a1 = fig.add_subplot(513)
    a2 = fig.add_subplot(514)
    a3 = fig.add_subplot(515)

    x = np.r_[:WINDOW]
    y = np.array([0]*WINDOW) #modified data
    z = np.array([0]*WINDOW) #raw data
    w = np.array([0]*WINDOW) #smoothed modified data

    y[0] = 15000
    z[0] = 15000
    w[0] = 15000

    #plt.ylim(-200, 6000)
    ly, = ax.plot(x, y)
    lz, = ax.plot(x, z)
    #plt.ylim(-200, 6000)
    lw, = aw.plot(x, w)
    la1, = a1.plot(x, w)
    la2, = a2.plot(x, w)
    la3, = a3.plot(x, w)

    aw.set_ylim([9000, 12000])
    a3.set_ylim([0, 3000])
    a2.set_ylim([3000, 6000])
    a1.set_ylim([6000, 9000])


    # draw and show it
    fig.canvas.draw()
    
    plt.show(block=False)

    prev = z[-1]
    bucket_no = 0
    i = 0
    j = 0
   
    smoothing_buffer = []
    for k in range(BUFFER_LENGTH):
        smoothing_buffer.append(y[-4 + i])

    while not rospy.is_shutdown():
        try:
            data = read()
            #data = [normalize(data[0], offset) + 2000]
            
            z = np.append(z, data)

            elem = z[-1]

            """
            if elem - prev > THRESHOLD:
                bucket_no -= 1
                bucket_no = max(-1, bucket_no)
            elif prev - elem > THRESHOLD:
                bucket_no += 1
                bucket_no = min(1, bucket_no)
            """
            
            #prev = elem
            #elem = transform(elem)

            modified_data_point = bucket_no * 4096 + elem + 2000
            y = np.append(y, modified_data_point)
                #12-bit input range from arduino
            w = np.append(w, sum(smoothing_buffer) / BUFFER_LENGTH)
            for k in range(BUFFER_LENGTH - 1):
                smoothing_buffer[k] = smoothing_buffer[k + 1]
            smoothing_buffer[BUFFER_LENGTH - 1] = elem # used to be modifed_data_point
            # print(z[-1])
            # print(y[-1])

            if i == 0:
                lz.set_ydata(z[-WINDOW:])
                ly.set_ydata(y[-WINDOW:])
                lw.set_ydata(w[-WINDOW:])
                la1.set_ydata(w[-WINDOW:])
                la2.set_ydata(w[-WINDOW:])
                la3.set_ydata(w[-WINDOW:])
                print('hi')
                fig.canvas.draw()
            i += 1
            i = i % REFRESH_RATE

            if j == 0:
                if status == START:
                    # smoothed data
                    palpation_data.append(w[-1])
                    recording_pose = True
            j += 1
            j = j % RECORD_RATE

# 1. find local peaks in signal
# 2. go out 5mm in each direction and save those points
# 3. do wls on the points (x, y from pose, weight from signal)



        except KeyboardInterrupt:
            break
    rospy.spin()

if __name__ == '__main__':
    listener()
