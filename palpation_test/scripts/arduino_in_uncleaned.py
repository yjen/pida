#!/usr/bin/python
from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
import serial, time
import rospy
from std_msgs.msg import Int32, String
from geometry_msgs.msg import PoseStamped

START = "start"
STOP = "stop"
THRESHOLD = 3700
#THRESHOLD2 = 3700
WINDOW = 700
REFRESH_RATE = 10
RECORD_RATE = 10 # 1 out of 10 data points will be recorded
ser = serial.Serial('/dev/ttyACM0', 9600)#, timeout=0)
#ser = serial.Serial('/dev/tty0', 9600)#, timeout=0)

def linear_scale(y):
    return (4096/4) * y

#deg 6 poly, coeffs highest to lowest
deg6 = [9.389e-21,1.684e-17,-8.124e-13,3.597e-09,-5.512e-06,0.003552,-0.3207]
def inverse(y):
    x = 0
    for coeff in deg6:
        x += coeff + x*y
    return x

def transform(elem):
    return linear_scale(inverse(elem))


def read1():
    data = ser.readline()
    while True:
        while data.strip() == '':
            data = ser.readline()
            print 'no data'
        data = data.strip().split(',')[1]
        data = data.strip().split('\r')
        result = [int(x) for x in data if x != '']
        if len(result) != 0:
            return result
    return None

def read():
    try:
        return read1()
    except (ValueError, IndexError) as e:
        return read()


palpation_data = []
pose_data = []
status = None
recording_pose = False

def record_status_callback(data):
    global palpation_data
    global pose_data
    global status
    status = data.data
    rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.data)
    
    if status == START:
        palpation_data = []
        pose_data = []
    elif status == STOP:
        print(palpation_data)
        print(pose_data)
        print(len(palpation_data))
        print(len(pose_data))

def record_pose_callback(data):
    global pose_data
    global recording_pose
    if recording_pose:
        pose_data.append(data)
        recording_pose = False


def listener():
    global palpation_data
    global recording_pose
    rospy.init_node('arduino_in', anonymous=True)
    rospy.Subscriber("palpation_record", String, record_status_callback)
    # /dvrk_psm1/joint_position_cartesian
    rospy.Subscriber("dvrk_psm1/joint_position_cartesian", PoseStamped, record_pose_callback)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ay = fig.add_subplot(111)


    x = np.r_[:WINDOW]
    y = np.array([0]*WINDOW) #modified data
    z = np.array([0]*WINDOW) #raw data

    y[0] = 15000 # used to be 15000
    z[0] = 15000 # used to be 15000

    li, = ax.plot(x, y)
    lz, = ax.plot(x, z)
    # draw and show it
    fig.canvas.draw()
    plt.show(block=False)

    prev2 = y[-2]
    prev = y[-1]
    bucket_no = 0
    i = 0
    j = 0
    """
    time.sleep(1)
    offset = 0
    for c in range(200):
        offset += read()[0]
    offset /= 200
    offset += 500
    print(offset)
    """
    
    """
    def normalize(number, offset):
        result = number - offset
        if result < 0:
            result = 4096 + result
        return result
    """

    while not rospy.is_shutdown():
        try:
            data = read()
            #data = [normalize(data[0], offset) + 2000]
            
            z = np.append(z, data)

            elem = z[-1]

            """
        
            if elem - prev > THRESHOLD2 and prev2 - prev > THRESHOLD2:
                bucket_no -= 1
                #bucket_no = max(0, bucket_no)
            elif prev - elem > THRESHOLD2 and prev - prev2 > THRESHOLD2:
                bucket_no += 1
                #bucket_no = min(1, bucket_no)
            elif elem - prev > THRESHOLD:
                bucket_no -= 1
                #bucket_no = max(0, bucket_no)
            elif prev - elem > THRESHOLD:
                bucket_no += 1
                #bucket_no = min(1, bucket_no)
            """

            if elem - prev > THRESHOLD:
                bucket_no -= 1
                bucket_no = max(-1, bucket_no)
            elif prev - elem > THRESHOLD:
                bucket_no += 1
                bucket_no = min(1, bucket_no)

            """
            if elem - prev > THRESHOLD and prev2 - prev > THRESHOLD:
                bucket_no -= 1
                bucket_no = max(0, bucket_no)
            elif prev - elem > THRESHOLD and prev - prev2 > THRESHOLD:
                bucket_no += 1
                bucket_no = min(1, bucket_no)
            """
            # so the problem is...how do you tell the difference between real jumps and fake jumps

            # look for big concave/convex jumps?
            # prev2 = prev
            prev = elem
            elem = transform(elem)

            # bucket_no * 4096 + elem is each individual point of data from palpation probe
            
            y = np.append(y, bucket_no * 4096 + elem + 4000)
                #12-bit input range from arduino

            
            # print(z[-1])
            # print(y[-1])

            if i == 0:
                lz.set_ydata(z[-WINDOW:])
                li.set_ydata(y[-WINDOW:])
                fig.canvas.draw()
            i += 1
            i = i % REFRESH_RATE

            if j == 0:
                if status == START:
                    palpation_data.append(bucket_no * 4096 + elem + 4000)
                    recording_pose = True
            j += 1
            j = j % RECORD_RATE

        except KeyboardInterrupt:
            break
    rospy.spin()

if __name__ == '__main__':
    listener()