import numpy as np

vein_p1 = np.array([0, 0, 0])
vein_p2 = np.array([1, 0, 0])
p1 = np.array([0, 1, 0])
p2 = np.array([0, 2, 0])
p3 = np.array([0, 3, 0]) 
p4 = np.array([1, 0, 0])
p5 = np.array([2, 0, 0])
p6 = np.array([0, 0, 1])
p7 = np.array([0, 0, 2])
p8 = np.array([1, 1, 0])
p9 = np.array([0, 1, 1])
p10 = np.array([1, 1, 1])
points = [p1, p2, p3, p4, p5, p6, p7, p8, p9, p10]

sum_errors = 0
for p0 in points:
	term1 = p0 - vein_p1
	term2 = p0 - vein_p2
	term3 = vein_p2 - vein_p1
	numerator = np.linalg.norm(np.cross(term1, term2))
	denominator = np.linalg.norm(term3)
	print(str(numerator / denominator) + " indiv error")
	sum_errors += numerator / denominator