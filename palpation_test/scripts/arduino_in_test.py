#!/usr/bin/python

import rospy
from std_msgs.msg import String

def talker():
  pub = rospy.Publisher("palpation_record", String, queue_size=10)
  rospy.init_node('arduino_in_tester', anonymous=True)
  while not rospy.is_shutdown():
    user_input = raw_input('Enter "record" or "stop"')
    pub.publish(user_input)

if __name__ == '__main__':
  try:
    talker()
  except rospy.ROSInterruptException:
    pass
