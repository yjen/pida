#!/usr/bin/python

import serial, time
import rospy
from std_msgs.msg import Int32, String


ser = serial.Serial('/dev/ttyACM0', 9600, writeTimeout=0)

class ArduinoIn:

    def __init__(self):
        self.position = -1
        self.ready = True


    def callback(self, data):
        # don't pull back as much, otherwise skin will split
        # reduce distance between injection lines
        # increase delay between second inject and when it starts moving
        rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.data)
        command = data.data.split()
        # 1.5 "mL" with stepsize of 3 and delay of 25.
        # have a method that you run before you put syringe in, then keep track of motor position from there
        # need 700 to get it to endrange position, 1400 (inject 2.5) for 3ml mark aaaaand it's broken gg
        if command[0] == 'syringe':
            if command[1] == 'init':
                #raw_input("check that the syringe is out of the holder. press any button to continue") # this can be done by caller too
                # so going through python is slow af
                ser.write('1,-13000,25')
                time.sleep(5)
                ser.write('1,6800,25') # 2.1mL mark on 3mL syringe
                self.ready = True
                self.position = 2000
                #raw_input("put syringe in. press any button to continue") # this can be done by caller too
            else:
                if self.ready:
                    volume = float(command[1])
                    if self.position + (1690 * volume) >= 0:
                        self.position += 1690 * volume
                        steps = 1690 * volume # for 3mL syringe. should be 560 for 10mL syringe.
                        ser.write('1,' + str(int(steps)) + ',6') # used to be 3
                    else:
                        print("this will pull the syringe back too far")
                else:
                    print('please init syringe first')
        elif command[0] == 'rack':
            if command[1] == 'lock':
                ser.write('2,0')
            elif command[1] == 'unlock':
                ser.write('2,45')
        elif command[0] == 'probe':
            if command[1] == 'start':
                ser.write('3')
            elif command[1] == 'stop':
                ser.write('0')

    def listener(self):
        rospy.init_node('automated_tools', anonymous=True)
        rospy.Subscriber("tool_commands", String, self.callback)
        rospy.spin()

if __name__ == '__main__':
    a = ArduinoIn()
    a.listener()
